//
//  TrackingViewController.swift
//  SoleSearching
//
//  Created by Daniel Marchese on 1/31/15.
//  Copyright (c) 2015 Daniel Marchese. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import AudioToolbox
import CoreBluetooth

class TrackingViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, CBCentralManagerDelegate {
    
    let defaultRadius: CLLocationDistance = CLLocationDistance(30)

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    
    var manager: CLLocationManager!
    var centralManager: CBCentralManager!
    var peripheral: CBPeripheral!
    
    var route: MKRoute!
    var destination: CLLocationCoordinate2D!
    var currentStep: MKRouteStep?
    var stepIndex: Int = 0
    var overlay: MKCircle!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
    }

    override func viewDidAppear(animated: Bool) {
        // Register the first region.
        self.currentStep = self.route.steps[1] as? MKRouteStep
        self.stepIndex = 1
        self.overlay = MKCircle(centerCoordinate: self.currentStep!.polyline.coordinate, radius: defaultRadius)
        self.mapView.addOverlay(self.overlay, level: .AboveRoads)
        
        // Set up the location manager.
        self.manager = CLLocationManager()
        self.manager.delegate = self
        self.manager.desiredAccuracy = kCLLocationAccuracyBest
        self.manager.distanceFilter = 10
        self.manager.startUpdatingLocation()
        
        super.viewDidAppear(animated)
    }
    
    @IBAction func onRefreshSelected(sender: AnyObject) {
        self.centralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
    }
    
    // MARK: - MKMapViewDelegate
    
    func mapView(mapView: MKMapView!, didAddAnnotationViews views: [AnyObject]!) {
        for annotationView in views {
            if let annotation = annotationView.annotation as? MKUserLocation {
                if annotation == mapView.userLocation {
                    
                    // Zoom to the user's location
                    let region: MKCoordinateRegion = MKCoordinateRegion(center: mapView.userLocation.coordinate, span: MKCoordinateSpanMake(0.03, 0.03))
                    mapView.setRegion(region, animated: true)
                    mapView.regionThatFits(region)
                    
                    // Draw the route
                    self.mapView.addOverlay(self.route.polyline, level: .AboveRoads)
                }
            }
        }
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if let ovly = overlay as? MKPolyline {
            let renderer: MKPolylineRenderer = MKPolylineRenderer(polyline: ovly)
            renderer.strokeColor = UIColor.orangeColor()
            renderer.alpha = 0.7
            return renderer
        } else if let ovly = overlay as? MKCircle {
            let renderer: MKCircleRenderer = MKCircleRenderer(circle: ovly)
            renderer.strokeColor = UIColor.orangeColor()
            renderer.alpha = 0.3
            return renderer
        } else {
            return nil
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    
    // This method needs to be able to run in the background.
    func locationManager(manager: CLLocationManager!, didEnterRegion region: CLRegion!) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        // Buzz the shoes.
        self.centralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
        self.centralManager.scanForPeripheralsWithServices([CBUUID(string: BT_UUID_VALUE)], options: nil)
        
        // Move to the next step in the sequence.
        self.stepIndex++
        if self.stepIndex < self.route.steps.count-1 {
            self.currentStep = self.route.steps[self.stepIndex] as? MKRouteStep
            let circle: MKCircle = MKCircle(centerCoordinate: self.currentStep!.polyline.coordinate, radius: CLLocationDistance(100))
            self.mapView.addOverlay(circle, level: .AboveRoads)
        } else {
            // We just show the destination for this point.
            self.currentStep = self.route.steps[-1] as? MKRouteStep
            let circle: MKCircle = MKCircle(centerCoordinate: self.destination, radius: CLLocationDistance(100))
            self.mapView.addOverlay(circle, level: .AboveRoads)
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        if locations.count > 0 {
            if let location = locations.last as? CLLocation {
                // Check the distance from the next POI
                let poiCoordinates: CLLocationCoordinate2D = self.currentStep!.polyline.coordinate
                if location.distanceFromLocation(CLLocation(latitude: poiCoordinates.latitude, longitude: poiCoordinates.longitude)) < defaultRadius {
                    // We're inside the circle, here we go...
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    
                    // TODO: Buzz the shoes.
                    //self.centralManager.scanForPeripheralsWithServices([BT_UUID_VALUE, RX_UUID_VALUE, TX_UUID_VALUE, BG_UUID_VALUE, RE_UUID_VALUE, MN_UUID_VALUE], options: nil)
                    
                    // Remove the old circle.
                    self.mapView.removeOverlay(self.overlay)
                    self.overlay = nil
                    
                    // Move to the next step in the sequence.
                    self.stepIndex++
                    if self.stepIndex < self.route.steps.count {
                        self.currentStep = self.route.steps[self.stepIndex] as? MKRouteStep
                        self.overlay = MKCircle(centerCoordinate: self.currentStep!.polyline.coordinate, radius: self.defaultRadius)
                        self.mapView.addOverlay(self.overlay, level: .AboveRoads)
                    }
                }
            }
        }
    }
    
    // MARK: - CBCentralManagerDelegate
    
    func centralManagerDidUpdateState(central: CBCentralManager!) {
        if central.state == .PoweredOn {
            self.centralManager.scanForPeripheralsWithServices([CBUUID(string: BT_UUID_VALUE)], options: nil)
        }
    }
    
    func centralManager(central: CBCentralManager!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [NSObject : AnyObject]!, RSSI: NSNumber!) {
        // Connect to the given peripheral.
        if self.peripheral == nil {
            self.peripheral = peripheral
            self.centralManager.connectPeripheral(self.peripheral, options: nil)
            println("DEVICES AHHHHH!!!!")
        }
    }
    
    func centralManager(central: CBCentralManager!, didConnectPeripheral peripheral: CBPeripheral!) {
        println("HOLY FUCKING SHIT!!!!!")
        self.centralManager.cancelPeripheralConnection(self.peripheral)
    }
    
    func centralManager(central: CBCentralManager!, didDisconnectPeripheral peripheral: CBPeripheral!, error: NSError!) {
        self.peripheral = nil
    }
}
