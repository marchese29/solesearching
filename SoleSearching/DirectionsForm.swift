//
//  DirectionsForm.swift
//  SoleSearching
//
//  Created by Daniel Marchese on 1/30/15.
//  Copyright (c) 2015 Daniel Marchese. All rights reserved.
//

import Foundation

class DirectionsForm : NSObject, FXForm {
    
    var location: NSString?
    
    func fields() -> [AnyObject]! {
        return [[FXFormFieldKey: "location", FXFormFieldHeader: "Data"]]
    }
}
