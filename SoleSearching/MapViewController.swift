//
//  MapViewController.swift
//  SoleSearching
//
//  Created by Daniel Marchese on 1/30/15.
//  Copyright (c) 2015 Daniel Marchese. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var undoButton: UIBarButtonItem!
    @IBOutlet weak var startButton: UIBarButtonItem!
    @IBOutlet weak var routeButton: UIBarButtonItem!
    
    var annotation: MKPointAnnotation?
    var location: CLLocationCoordinate2D?
    var route: MKRoute?
    var destination: CLLocationCoordinate2D?
    
    var pinCount: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self

        // Do any additional setup after loading the view.
    }
    
    func handleLongPress(sender: UIGestureRecognizer) {
        if sender.state == .Ended {
            self.mapView.removeGestureRecognizer(sender)
        } else if self.pinCount != 1 {
            self.pinCount = 1
            let point: CGPoint = sender.locationInView(self.mapView)
            self.location = self.mapView.convertPoint(point, toCoordinateFromView: self.mapView)
            self.annotation = MKPointAnnotation()
            self.annotation!.coordinate = self.location!
            self.mapView.addAnnotation(self.annotation)
        }
    }
    
    // MARK: Actions
    
    @IBAction func onStartSelected(sender: AnyObject) {
        // TODO: Check bluetooth connections, start navigation.
        if let route = self.route {
            self.performSegueWithIdentifier("showTracking", sender: self)
        }
    }
    
    @IBAction func onUndoSelected(sender: AnyObject) {
        if self.annotation != nil && self.location != nil {
            self.mapView.removeAnnotation(self.annotation)
            self.mapView.removeOverlay(self.route!.polyline)
            self.annotation = nil
            self.location = nil
            self.route = nil
            self.pinCount = 0
            self.destination = nil
        
            // Re-configure the gesture recognizer.
            let recognizer: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: "handleLongPress:")
            recognizer.minimumPressDuration = 0.8
            mapView.addGestureRecognizer(recognizer)
        }
    }

    @IBAction func onRouteSelected(sender: AnyObject) {
        if let loc = location {
            let request: MKDirectionsRequest = MKDirectionsRequest()
            request.setSource(MKMapItem.mapItemForCurrentLocation())
            request.setDestination(MKMapItem(placemark: MKPlacemark(coordinate: loc, addressDictionary: nil)))
            request.transportType = MKDirectionsTransportType.Walking
            let directions: MKDirections = MKDirections(request: request)
            
            directions.calculateDirectionsWithCompletionHandler { (response: MKDirectionsResponse!, error: NSError!) -> Void in
                self.route = response.routes[0] as? MKRoute
                self.destination = loc
                if let route = self.route {
                    self.mapView.addOverlay(route.polyline, level: .AboveRoads)
                    // Zoom to fit the line.
                    let boundingRect: MKMapRect = route.polyline.boundingMapRect
                    self.mapView.setRegion(MKCoordinateRegionForMapRect(route.polyline.boundingMapRect), animated: true)
                }
            }
        }
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showTracking" && self.route != nil && self.destination != nil {
            let navController: UINavigationController = segue.destinationViewController as UINavigationController
            let trackingController: TrackingViewController = navController.topViewController as TrackingViewController
            trackingController.route = self.route!
            trackingController.destination = self.destination!
        }
    }
    
    // MARK: - MKMapViewDelegate
    
    // This causes the map to automatically zoom to the user's location.
    func mapView(mapView: MKMapView!, didAddAnnotationViews views: [AnyObject]!) {
        for annotationView in views {
            if let annotation = annotationView.annotation as? MKUserLocation {
                if annotation == mapView.userLocation {
                
                    // Zoom to the user's location
                    let region: MKCoordinateRegion = MKCoordinateRegion(center: mapView.userLocation.coordinate, span: MKCoordinateSpanMake(0.03, 0.03))
                    mapView.setRegion(region, animated: true)
                    mapView.regionThatFits(region)
                
                    // Configure the long-press gesture recognizer.
                    let recognizer: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: "handleLongPress:")
                    recognizer.minimumPressDuration = 0.8
                    mapView.addGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    // Renders the route
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if let ovly = overlay as? MKPolyline {
            let renderer: MKPolylineRenderer = MKPolylineRenderer(polyline: ovly)
            renderer.strokeColor = UIColor.orangeColor()
            return renderer
        } else {
            return nil
        }
    }

}
