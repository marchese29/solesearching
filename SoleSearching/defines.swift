//
//  defines.swift
//  SoleSearching
//
//  Created by Daniel Marchese on 2/1/15.
//  Copyright (c) 2015 Daniel Marchese. All rights reserved.
//

import Foundation
import CoreBluetooth

let BG_UUID_VALUE = "8E292BDD-1462-7179-BE20-246B21988161"
let BT_UUID_VALUE = "195AE58A-437A-489B-B0CD-B7C9C394BAE4"
let RX_UUID_VALUE = "5FC569A0-74A9-4FA4-B8B7-8354C86E45A4"
let TX_UUID_VALUE = "21819AB0-C937-4188-B0D8-B9621E1696CD"
let RE_UUID_VALUE = "2A29"
let MN_UUID_VALUE = "2A24"
